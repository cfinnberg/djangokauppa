from django.db import models
from django.utils.translation import gettext as _
from djangokauppa import settings

# Create your models here.
# var tuoteSchema = new Schema({
#     tuoteid: { type: String, required: true, unique: true },
#     nimi: { type: String, required: true },
#     hinta: { type: Number, required: true },
#     lisatiedot: String,
#     valokuvat: [valokuvaSchema],
#     created_at: Date,
#     updated_at: Date
# });

class Tuote(models.Model):
    nimi = models.CharField(max_length=100)
    hinta = models.DecimalField(max_digits=6, decimal_places=2, null=False)
    lisatiedot = models.TextField(_("Lisätiedot"))
    luotu = models.DateField(_("Lisätty"), auto_now=False, auto_now_add=True)
    paivitetty = models.DateField(_("Viimeksi päivitetty"), auto_now=True)
    määrä = models.IntegerField(_("Määrä"), default=0, null=False)
    kuva = models.ImageField(_('Kuva'), upload_to='images', null=False)

    def __str__(self):
        return self.nimi

    class Meta:
        verbose_name_plural = "Tuotteet"

class OstosKärry(models.Model):
    käyttäjä = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("Käyttäjän ID"), on_delete=models.CASCADE, null=False, blank=False)
    tuote = models.ForeignKey(Tuote, verbose_name=_("Tuote ID"), on_delete=models.CASCADE, null=False, blank=False)
    määrä = models.IntegerField(_("Määrä"), default=1, null=False)

    def kokonaishinta(self):
        return self.tuote.hinta * self.määrä

    def __str__(self):
        return f'{self.käyttäjä.first_name} - {self.tuote.nimi} x {self.määrä}'

class Ostos(models.Model):
    ostos_pvm = models.DateTimeField(_("Ostoksen PVM"), auto_now=False, auto_now_add=True)
    kokonaishinta = models.DecimalField(max_digits=6, decimal_places=2, null=False)
    käyttäjä = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("Käyttäjän ID"), on_delete=models.CASCADE, null=False, blank=False)

    def __str__(self):
        return f'{self.käyttäjä} ({self.ostos_pvm}): {self.kokonaishinta}'
    
class OstoksenTuote(models.Model):
    ostos = models.ForeignKey(Ostos, verbose_name=_("Ostos"), on_delete=models.CASCADE, null=False, blank=False)
    tuote = models.ForeignKey(Tuote, verbose_name=_("Tuote"), on_delete=models.CASCADE, null=False, blank=False)
    määrä = models.IntegerField(_("Määrä"), null=False)
    hinta_per_kpl = models.DecimalField(max_digits=6, decimal_places=2, null=False)

    def __str__(self):
        return f'{self.ostos.käyttäjä.first_name} - {self.tuote.nimi} x {self.määrä}'

