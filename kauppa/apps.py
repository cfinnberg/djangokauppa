from django.apps import AppConfig


class KauppaConfig(AppConfig):
    name = 'kauppa'
