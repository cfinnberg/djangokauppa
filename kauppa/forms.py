from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import gettext as _
from asiakas.forms import ProfiiliCreationForm
from asiakas.models import Profiili


#
# Käyttäjän rekisteröinti
#
class RekisteröintiLomake(ProfiiliCreationForm):
    first_name = forms.CharField(label='Etunimi', max_length=30, required=False, help_text='Etunimi')
    last_name = forms.CharField(label='Sukunimi', max_length=30, required=False, help_text='Sukunimi')
    email = forms.EmailField(label='Sähköposti', max_length=254, help_text='Laita oikea sähköpostiosoittetta')

    class Meta:
        model = Profiili
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )
