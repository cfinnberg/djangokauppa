from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import Tuote, OstosKärry, Ostos, OstoksenTuote
from django import forms
import random
from django.contrib.auth.decorators import login_required
from django.urls import reverse, reverse_lazy
from datetime import datetime, timezone
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from .forms import RekisteröintiLomake


#
# Apufunktiota
#
def laske_tuotteidenmäärä(user):
    ostoslista = OstosKärry.objects.filter(käyttäjä=user)
    if not ostoslista:
        return 0
    määrä = 0
    for item in ostoslista:
        määrä += item.määrä
    return määrä

def laske_kokonaishinta(user):
    ostoslista = OstosKärry.objects.filter(käyttäjä=user)
    if not ostoslista:
        return 0
    hinta = 0
    for item in ostoslista:
        hinta += item.tuote.hinta * item.määrä
    return hinta

#
# Etusivu
#
def etusivu(request):
    korostettu = random.choice(Tuote.objects.filter(määrä__gt="0"))
    context = { 'hero' : korostettu }
    if request.user.is_authenticated:
        context['ostosmäärä'] = laske_tuotteidenmäärä(request.user)
    return render(request, 'etusivu.html', context)

#
# Yhteystiedot
#
def yhteystiedot(request):
    context = {}
    if request.user.is_authenticated:
        context['ostosmäärä'] = laske_tuotteidenmäärä(request.user)
    # return HttpResponse('Yhteystiedot')
    return render(request, 'yhteystiedot.html', context)

#
# Tietosuojaseloste
#
def tietosuojaseloste(request):
    context = {}
    if request.user.is_authenticated:
        context['ostosmäärä'] = laske_tuotteidenmäärä(request.user)
    return render(request, 'tietosuojaseloste.html', context)

#
# Tuotteet
#
def tuotteet(request):
    context = { 'tuotteet': Tuote.objects.filter(määrä__gt="0") }
    if request.user.is_authenticated:
        context['ostosmäärä'] = laske_tuotteidenmäärä(request.user)
    return render(request, 'tuotteet.html', context)


# class UusiTuoteForm(forms.Form):
#     nimi = forms.CharField(label = "Tuotteen nimi:", max_length=100, required = True)
#     esitys = forms.CharField(label = "Tietoa tuotteesta:", widget=forms.Textarea, required = True)
#     hinta = forms.DecimalField(min_value = 0, decimal_places = 2, required = True)
#     määrä = forms.IntegerField(min_value = 0, required = True)

# def lisätä_tuote(request):
#     if request.method == 'POST':
#         tuoteForm = UusiTuoteForm(request.POST)
#         if tuoteForm.is_valid():
#             nimi = tuoteForm.cleaned_data['nimi']
#             esitys = tuoteForm.cleaned_data['esitys']
#             hinta = tuoteForm.cleaned_data['hinta']
#             määrä = tuoteForm.cleaned_data['määrä']
#             tuote = Tuote(nimi=nimi, lisatiedot=esitys, hinta=hinta, määrä=määrä)
#             tuote.save()
#         tuoteForm = NewTuoteForm()
#         return render(request, 'addtuote.html', {'form': tuoteForm})
#     else:
#         tuoteForm = NewTuoteForm()
#         return render(request, 'addtuote.html', {'form': tuoteForm})
#     return redirect('etusivu')

#
# Lisätä tuote kärryyn
#
@login_required(login_url = reverse_lazy('login'))
def lisätä_kärryyn(request, tuoteid):
    tuote_kärryllä = OstosKärry.objects.filter(käyttäjä=request.user, tuote=tuoteid)
    tuote = Tuote.objects.get(id=tuoteid) ## TODO: Mitä jos tuote ei ole olemassa???
    if tuote_kärryllä:
        tuote_kärryllä = tuote_kärryllä[0]
        if tuote.määrä >= tuote_kärryllä.määrä + 1:
            tuote_kärryllä.määrä += 1
    else:
        tuote_kärryllä = OstosKärry.objects.create(käyttäjä=request.user, tuote=tuote)
    tuote_kärryllä.save()
    if 'referer' in request.headers:
        next = request.headers.get('referer')
    else:
        next = reverse(etusivu)
    return redirect(next)
    # return HttpResponse(f'OK: id={id}, user={request.user.id}')

#
# Poista tuote kärrystä (poista 1)
#
@login_required(login_url = reverse_lazy('login'))
def poistaa_kärrystä(request, tuoteid):
    tuote_kärryllä = OstosKärry.objects.filter(käyttäjä=request.user, tuote=tuoteid)
    if tuote_kärryllä:
        tuote_kärryllä = tuote_kärryllä[0]
        tuote_kärryllä.määrä -= 1
        if tuote_kärryllä.määrä ==  0:
            tuote_kärryllä.delete()
        else:
            tuote_kärryllä.save()
    if 'referer' in request.headers:
        next = request.headers.get('referer')
    else:
        next = reverse(etusivu)
    return redirect(next)

#
# Poista tuotetyypi kärrystä
#
@login_required(login_url = reverse_lazy('login'))
def poistaatuote_kärrystä(request, tuoteid):
    tuote_kärryllä = OstosKärry.objects.filter(käyttäjä=request.user, tuote=tuoteid)
    if tuote_kärryllä:
        tuote_kärryllä[0].delete()
    if 'referer' in request.headers:
        next = request.headers.get('referer')
    else:
        next = reverse(etusivu)
    return redirect(next)

#
# Tuotteen yksityiskohdat
#
def tuote_yksityiskohdat(request, tuoteid):
    tuote = get_object_or_404(Tuote, id=tuoteid)
    context = { 'tuote': tuote }
    if request.user.is_authenticated:
        context['ostosmäärä'] = laske_tuotteidenmäärä(request.user)
    return render(request, 'yksityiskohdat.html', context)

#
# Ostöskärry
#
@login_required(login_url = reverse_lazy('login'))
def ostoskärry(request):
    ostoslista = OstosKärry.objects.filter(käyttäjä=request.user)
    context = { 
        'ostoslista': ostoslista,
        'ostosmäärä': laske_tuotteidenmäärä(request.user),
        'kokonaishinta': laske_kokonaishinta(request.user),
    }
    return render(request, 'ostoskärry.html', context)

#
# Ostaa
#
@login_required(login_url = reverse_lazy('login'))
def ostaa(request):
    ostoslista = OstosKärry.objects.filter(käyttäjä=request.user)
    if not ostoslista:
        return redirect('etusivu')
    ostos = Ostos.objects.create(
        käyttäjä=request.user,
        ostos_pvm = datetime.now(timezone.utc),
        kokonaishinta = laske_kokonaishinta(request.user)
    )
    for item in ostoslista:
        ostoksentuote = OstoksenTuote.objects.create(
            ostos = ostos,
            tuote = item.tuote,
            määrä = item.määrä,
            hinta_per_kpl = item.tuote.hinta
        )
        item.tuote.määrä -= item.määrä
        item.tuote.save()
        item.delete()
    return render(request, 'kiitos.html' , { 'ostosmäärä': 0})

#
# Rekisteröinti
#
def rekisteröinti(request):
    if request.method == 'POST':
        lomake = RekisteröintiLomake(request.POST)
        if lomake.is_valid():
            lomake.save()
            username = lomake.cleaned_data.get('username')
            raw_password = lomake.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('etusivu')
    else:
        lomake = RekisteröintiLomake()
    return render(request, 'rekisteröinti.html', {'form': lomake})
