from django.contrib import admin

# Register your models here.
from .models import Tuote

# admin.site.register(Tuote)

@admin.register(Tuote)
class TuoteAdmin(admin.ModelAdmin):
    list_display = ('nimi', 'hinta', 'määrä')
    ordering = ('nimi',)
    search_fields = ('nimi',)