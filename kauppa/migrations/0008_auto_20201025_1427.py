# Generated by Django 3.1.2 on 2020-10-25 12:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('kauppa', '0007_auto_20201023_1343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tuote',
            name='paivitetty',
            field=models.DateField(auto_now=True, verbose_name='Viimeksi päivitetty'),
        ),
        migrations.CreateModel(
            name='Ostos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ostos_pvm', models.DateTimeField(auto_now_add=True, verbose_name='Ostoksen PVM')),
                ('kokonaishinta', models.DecimalField(decimal_places=2, max_digits=6)),
                ('käyttäjä', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Käyttäjän ID')),
            ],
        ),
        migrations.CreateModel(
            name='OstoksenTuotteet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('määrä', models.IntegerField(verbose_name='Määrä')),
                ('hinta_per_kpl', models.DecimalField(decimal_places=2, max_digits=6)),
                ('ostos', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kauppa.ostos', verbose_name='Ostos')),
                ('tuote', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kauppa.tuote', verbose_name='Tuote')),
            ],
        ),
    ]
