# Generated by Django 3.1.2 on 2020-10-14 17:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kauppa', '0002_auto_20201012_1116'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tuote',
            options={'verbose_name_plural': 'Tuotteet'},
        ),
        migrations.AddField(
            model_name='tuote',
            name='määrä',
            field=models.IntegerField(default=0, verbose_name='Määrä'),
        ),
    ]
