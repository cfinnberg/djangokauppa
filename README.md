# Verkkokauppa project example

## Clone repository

```
git clone https://gitlab.com/cfinnberg/djangokauppa.git verkkokauppa
```

## Make it work

### Windows (with Powershell)

```
cd verkkokauppa
python -m venv venv
. .\venv\scripts\activate.ps1
python -m pip install -U pip
pip install -r requirements.txt
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

Note: 
- for **cmd** replace the activate line with `. .\venv\scripts\activate.bat`
- for **Linux** and **Mac** replace the activate line with `. ./venv/scripts/activate`

Open in the browser the address: [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin) and log in with the (super)user you just created and add at least one product. After that you can open the verkkokauppa's main page at http://127.0.0.1:8000/
