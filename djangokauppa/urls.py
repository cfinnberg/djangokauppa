"""djangokauppa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from kauppa import views
from django.contrib.auth import views as auth_views
from django.conf import settings 
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.etusivu, name='etusivu'),
    path('yhteystiedot', views.yhteystiedot, name='yhteystiedot'),
    path('tietosuojaseloste', views.tietosuojaseloste, name='tietosuojaseloste'),
    path('tuotteet', views.tuotteet, name='tuotteet'),
    path('tuote/<int:tuoteid>', views.tuote_yksityiskohdat, name='tuote_yksityiskohdat'),
    # path('tuote/lisätä',views.lisätä_tuote, name='lisätä_tuote'),
    path('kärry/lisätä/<int:tuoteid>', views.lisätä_kärryyn, name='lisätä_kärryyn'),
    path('kärry/poistaa/<int:tuoteid>', views.poistaa_kärrystä, name='poistaa_kärrystä'),
    path('kärry/poistaatuote/<int:tuoteid>', views.poistaatuote_kärrystä, name='poistaatuote_kärrystä'),
    path('ostoskärry', views.ostoskärry, name='ostoskärry'),
    path('ostaa', views.ostaa, name='ostaa'),
    path('login', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout', auth_views.LogoutView.as_view(next_page='etusivu'), name='logout'),
    path('signup', views.rekisteröinti, name='rekisteröinti'),
]

# This is needed in DEBUG mode for user uploaded images to work
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)