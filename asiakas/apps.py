from django.apps import AppConfig


class AsiakasConfig(AppConfig):
    name = 'asiakas'
