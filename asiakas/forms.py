from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Profiili

class ProfiiliCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = Profiili
        fields = UserCreationForm.Meta.fields

class ProfiiliChangeForm(UserChangeForm):

    class Meta(UserCreationForm.Meta):
        model = Profiili
        fields = UserCreationForm.Meta.fields
