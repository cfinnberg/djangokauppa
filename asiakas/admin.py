from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import ProfiiliCreationForm, ProfiiliChangeForm
from .models import Profiili

class ProfiiliAdmin(UserAdmin):
    add_form = ProfiiliCreationForm
    form = ProfiiliChangeForm
    model = Profiili
    list_display = ['email', 'username',]

admin.site.register(Profiili, ProfiiliAdmin)
